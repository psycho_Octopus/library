const crypto = require('crypto');
const R = require('ramda');
const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const UsersEnum = require('../../../shared/enums/roles');

const schema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please enter your name'],
    },
    email: {
        type: String,
        required: [true, 'Please enter your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please enter a valid email'],
    },
    avatar: String,
    role: {
        type: String,
        enum: R.values(UsersEnum),
        default: UsersEnum.User,
    },
    password: {
        type: String,
        required: [true, 'Please enter your password'],
        minlength: 8,
        select: false,
    },
    passwordConfirm: {
        type: String,
        required: [true, 'Please confirm your password'],
        validate: {
            validator(value) {
                return value === this.password;
            },
            message: "Passwords don't match",
        },
    },
    passwordChangedAt: Date,
    passwordResetToken: String,
    passwordResetExpires: Date,
});

schema.pre('save', async function preSave(next) {
    if (!this.isModified('password')) {
        next();
    }

    this.password = await bcrypt.hash(this.password, 12);
    this.passwordConfirm = undefined;

    next();
});

schema.method('comparePasswords', function comparePasswords(passToCheck, userPass) {
    return bcrypt.compare(passToCheck, userPass);
});

schema.method('hasChagedPasswordAfter', function hasChagedPasswordAfter(timestamp) {
    if (!this.passwordChangedAt) return false;

    const changedAt = parseInt(this.passwordChangedAt.getTime() / 1000, 10);

    return timestamp < changedAt;
});

schema.method('updatePasswordResetToken', function updatePasswordResetToken() {
    const resetToken = crypto.randomBytes(20).toString('hex');
    this.passwordResetToken = crypto
        .createHash('sha256')
        .update(resetToken)
        .digest('hex');
    this.passwordResetExpires = Date.now() + 10 * 60 * 1000;

    return resetToken;
});

module.exports = schema;
