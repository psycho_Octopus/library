const mongoose = require('mongoose');
const schema = require('./schema');

const Model = mongoose.model('User', schema);

module.exports = Model;
