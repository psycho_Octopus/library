// const _ = require('lodash');
const mongoose = require('mongoose');
const schema = require('./schema');

const Model = mongoose.model('Book', schema);

// const getAll = async ({ filter }) => {
//     let books = db.get('books');

//     if (filter.title) {
//         books = books.filter(book => book.title.toLowerCase().includes(filter.title.toLowerCase()));
//     }

//     if (filter.location) {
//         books = books.filter({ location: filter.location });
//     }

//     if (filter.tags && filter.tags.length > 0) {
//         books = books.filter(book => {
//             const matchedTags = _.intersection(book.tags, _.concat(filter.tags, []));

//             return matchedTags.length > 0;
//         });
//     }

//     return books.value();
// };

// const getOne = id => {
//     const book = db
//         .get('books')
//         .find({ id })
//         .value();

//     return book || null;
// };

// const updateOne = (id, patch) => {
//     db.get('books')
//         .find({ id })
//         .assign({ status: patch.status })
//         .write();

//     return getOne(id);
// };

// const deleteOne = id => {
//     const bookToDelete = { ...getOne(id) };

//     db.get('books')
//         .remove({ id })
//         .write();

//     return bookToDelete;
// };

// const getAllTags = () => {
//     const tags = db
//         .get('books')
//         .map(book => book.tags)
//         .flatMap()
//         .sortBy(book => book)
//         .sortedUniq()
//         .value();

//     return tags;
// };

// const orderOne = ({ title, location }) => {
//     return Model.create({
//         title,
//         location,
//     });
// };

module.exports = Model;

// {
//     getAll,
//     getOne,
//     updateOne,
//     deleteOne,
//     getAllTags,
//     orderOne,
//     Model,
// };
