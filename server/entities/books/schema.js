const mongoose = require('mongoose');
const slugify = require('slugify');

const schema = new mongoose.Schema({
    status: {
        type: Number,
        enum: [1, 2, 3],
        // enum: ['waited', 'available', 'occupied'],
        default: 1,
    },
    title: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        minlength: 4,
        maxlength: 100,
    },
    slug: {
        type: String,
        unique: true,
        maxlength: 120,
    },
    description: {
        type: String,
        trim: true,
        maxlength: 500,
    },
    fullDescription: {
        type: String,
        trim: true,
        maxlength: 2500,
    },
    cover: {
        type: String,
        default: '',
        maxlength: 200,
        validate(value) {
            return /\.(jpeg|jpg|png|webp)$/i.test(value);
        },
    },
    location: {
        type: String,
        enum: ['spb', 'prg'],
        required: true,
    },
    tags: {
        type: [String],
        default: [],
    },
    ratingAverage: {
        type: Number,
        default: 0,
        min: 0,
        max: 5,
    },
    ratingQuantity: {
        type: Number,
        default: 0,
        min: 0,
    },
});

schema.pre('save', function preSave(next) {
    this.slug = slugify(this.title, {
        lower: true,
    });

    next();
});

module.exports = schema;
