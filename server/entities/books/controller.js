const R = require('ramda');

const catchAsync = require('../../utils/catchAsync');

const Book = require('./model');

const getAll = catchAsync(async (req, res) => {
    const { query } = req;

    // BUILD DB QUERY
    const dbQuery = Book.find();

    // 1) Filtering
    if (query.status) {
        dbQuery.where('status', parseInt(query.status, 10));
    }

    if (query.title) {
        dbQuery.where('title', new RegExp(query.title, 'ig'));
    }

    if (query.location) {
        dbQuery.where('location', query.location);
    }

    if (query.tags) {
        dbQuery.where({ tags: { $elemMatch: { $in: query.tags } } });
    }

    // 2) Sorting
    const sortWhiteList = ['title'];

    if (query.sort && R.includes(query.sort, sortWhiteList)) {
        dbQuery.sort(query.sort);
    } else {
        dbQuery.sort('title');
    }

    // 3) Fields
    if (query.fields) {
        const fields = query.fields.replace(/,/gi, ' ');

        dbQuery.select(fields);
    } else {
        dbQuery.select('-__v');
    }

    // 4) Pagination
    if (query.offset) {
        dbQuery.skip(parseInt(query.offset, 10));
    }

    if (query.limit) {
        dbQuery.limit(parseInt(query.limit, 10));
    } else {
        dbQuery.limit(10);
    }

    // EXEC DB QUERY
    const result = await dbQuery;

    // SEND RESPONSE
    return res.json({
        status: 'success',
        result: result.length,
        data: result,
    });
});

const getCount = catchAsync(async (req, res) => {
    const result = await Book.count();

    res.json({
        status: 'success',
        data: result,
    });
});

const getOne = catchAsync(async (req, res) => {
    const bookId = req.params.id;
    const book = await Book.findById(bookId);

    res.json({
        status: 'success',
        data: book,
    });
});

const updateOne = catchAsync(async (req, res) => {
    const bookId = req.params.id;
    const book = await Book.findByIdAndUpdate(
        bookId,
        {
            status: req.body.status,
        },
        {
            new: true,
            runValidators: true,
        },
    );

    res.json({
        status: 'success',
        data: book,
    });
});

const deleteOne = catchAsync(async (req, res) => {
    const bookId = req.params.id;
    const book = await Book.findByIdAndDelete(bookId);

    return res.json({
        status: 'success',
        data: book,
    });
});

const getAllTags = catchAsync(async (req, res) => {
    const { location } = req.query;

    const result = await Book.aggregate(
        [
            location
                ? {
                      $match: {
                          location,
                      },
                  }
                : false,
            {
                $unwind: '$tags',
            },
            {
                $group: {
                    _id: '$tags',
                    count: { $sum: 1 },
                },
            },
            {
                $addFields: { name: '$_id' },
            },
            {
                $project: { _id: 0 },
            },
            {
                $sort: { name: 1 },
            },
        ].filter(Boolean),
    );

    res.json({
        status: 'success',
        data: result,
    });
});

const makeOrder = catchAsync(async (req, res) => {
    const book = await Book.create(req.body);

    return res.status(201).json({
        status: 'success',
        data: book,
    });
});

module.exports = {
    getAll,
    getCount,
    getOne,
    updateOne,
    deleteOne,
    getAllTags,
    makeOrder,
};
