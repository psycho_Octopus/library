const model = require('./model');
const router = require('./router');

module.exports = {
    model,
    router,
};
