const express = require('express');
const authController = require('../auth/controller');
const booksController = require('./controller');

const router = express.Router();

router
    .route('/')
    .get(booksController.getAll)
    .post(authController.requireLogin, booksController.makeOrder);

router.route('/count').get(booksController.getCount);
router.route('/tags').get(booksController.getAllTags);

router
    .route('/:id')
    .get(booksController.getOne)
    .patch(authController.requireLogin, booksController.updateOne)
    .delete(
        authController.requireLogin,
        authController.restrictTo('admin'),
        booksController.deleteOne,
    );

module.exports = router;
