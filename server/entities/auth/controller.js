const { promisify } = require('util');
const jwt = require('jsonwebtoken');
const R = require('ramda');
const UserModel = require('../users/model');
const catchAsync = require('../../utils/catchAsync');
const GlobalError = require('../../utils/errors/GlobalError');
const AuthError = require('../../utils/errors/AuthError');

const signToken = userId => {
    return jwt.sign(
        {
            id: userId,
        },
        process.env.JWT_SECRET,
        {
            expiresIn: process.env.JWT_EXPIRES_IN,
        },
    );
};

const signup = catchAsync(async (req, res) => {
    const newUser = await UserModel.create(req.body);
    const token = signToken(newUser._id);

    res.status(201)
        .cookie('jwt', token, {
            maxAge: process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000,
            secure: process.env.NODE_ENV === 'production',
            httpOnly: true,
        })
        .json({
            status: 'success',
            data: newUser,
        });
});

const login = catchAsync(async (req, res, next) => {
    const { email, password } = req.body;

    if (!email || !password) {
        return next(new AuthError(400));
    }

    const user = await UserModel.findOne({ email }).select('+password');

    if (!user) {
        return next(new AuthError(404));
    }

    const isPassMatch = await user.comparePasswords(password, user.password);

    if (!isPassMatch) {
        return next(new AuthError(401));
    }

    const token = signToken(user._id);

    res.json({
        status: 'success',
        token,
    });
});

const isJwtAuth = R.ifElse(R.identity, R.startsWith('Bearer'), R.always(false));
const extractToken = R.compose(
    R.last,
    R.split(' '),
);

const requireLogin = catchAsync(async (req, res, next) => {
    const { authorization } = req.headers;
    const token = isJwtAuth(authorization) ? extractToken(authorization) : undefined;

    if (!token) {
        return next(new GlobalError('Not logged in', 401));
    }

    const decodedToken = await promisify(jwt.verify)(token, process.env.JWT_SECRET);
    const currentUser = await UserModel.findById(decodedToken.id);

    if (!currentUser) {
        return next(new GlobalError("User doesn't exist", 401));
    }

    if (currentUser.hasChagedPasswordAfter(decodedToken.iat)) {
        return next(new GlobalError('Password has changed', 401));
    }

    req.user = currentUser;

    next();
});

const restrictTo = role => (req, res, next) => {
    const { user } = req;

    if (user.role !== role) {
        next(new GlobalError('Permission denied', 403));
    }

    next();
};

module.exports = {
    signup,
    login,
    requireLogin,
    restrictTo,
};
