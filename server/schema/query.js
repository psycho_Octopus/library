const { GraphQLObjectType, GraphQLList, GraphQLString, GraphQLInt } = require("graphql");
const R = require("ramda");
const db = require("../db");
const Book = require("./Book");

const filterByTitle = title =>
  R.compose(
    R.includes(R.toLower(title)),
    R.toLower,
    R.prop("title"),
  );

const includeTakenBy = ({ takenBy, ...book }) => {
  const takenByUser = db.get("users").find({ id: takenBy });

  return {
    ...book,
    takenBy: takenByUser.value() || null,
  };
};

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    books: {
      type: new GraphQLList(Book),
      args: {
        title: {
          type: GraphQLString,
        },
      },
      resolve(_, { title }) {
        const books = db.get("books");

        if (!title) {
          return books.value().map(includeTakenBy);
        }

        return R.filter(filterByTitle(title), books)
          .value()
          .map(includeTakenBy);
      },
    },
    book: {
      type: Book,
      args: {
        id: {
          type: GraphQLInt,
        },
      },
      resolve(_, { id }) {
        const bookValue = db
          .get("books")
          .find({ id })
          .value();

        return includeTakenBy(bookValue);
      },
    },
  },
});

module.exports = RootQuery;
