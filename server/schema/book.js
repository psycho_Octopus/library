const { GraphQLObjectType, GraphQLEnumType, GraphQLString, GraphQLInt } = require("graphql");
const User = require("./User");

const statusEnum = new GraphQLEnumType({
  name: "statusEnum",
  values: {
    WAITED: {
      value: "WAITED",
    },
    AVAILABLE: {
      value: "AVAILABLE",
    },
    OCCUPIED: {
      value: "OCCUPIED",
    },
  },
});

const locationEnum = new GraphQLEnumType({
  name: "locationEnum",
  values: {
    AMSTERDAM: {
      value: 1,
    },
    ROME: {
      value: 2,
    },
  },
});

const Book = new GraphQLObjectType({
  name: "Book",
  fields: {
    id: {
      type: GraphQLInt,
    },
    status: {
      type: statusEnum,
    },
    title: {
      type: GraphQLString,
    },
    slug: {
      type: GraphQLString,
    },
    description: {
      type: GraphQLString,
    },
    fullDescription: {
      type: GraphQLString,
    },
    cover: {
      type: GraphQLString,
    },
    location: {
      type: locationEnum,
    },
    ratingAverage: {
      type: GraphQLInt,
    },
    ratingQuantity: {
      type: GraphQLInt,
    },
    takenBy: {
      type: User,
    },
  },
});

module.exports = Book;
