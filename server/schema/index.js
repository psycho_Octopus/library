const { GraphQLSchema } = require("graphql");

const RootQuery = require("./query");

const schema = new GraphQLSchema({
  query: RootQuery,
});

module.exports = schema;
