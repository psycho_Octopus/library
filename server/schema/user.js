const { GraphQLObjectType, GraphQLString, GraphQLInt } = require("graphql");

const User = new GraphQLObjectType({
  name: "User",
  fields: {
    id: {
      type: GraphQLInt,
    },
    name: {
      type: GraphQLString,
    },
    avatar: {
      type: GraphQLString,
    },
  },
});

module.exports = User;
