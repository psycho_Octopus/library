const path = require('path');
const next = require('next');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const R = require('ramda');

const runServer = require('./server');

dotenv.config({
    path: path.join(__dirname, './config.env'),
});

const mongoUrl = R.compose(
    R.replace('<PASSWORD>', process.env.DATABASE_PASSWORD),
    R.replace('<USER>', process.env.DATABASE_USER),
)(process.env.DATABASE_URL);

mongoose.connect(mongoUrl, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
});

const port = parseInt(process.env.PORT, 10) || 3000;
const isDev = process.env.NODE_ENV !== 'production';
const app = next({
    dev: isDev,
    dir: path.join(__dirname, '..', 'src'),
});
const handle = app.getRequestHandler();

app.prepare().then(() => {
    const server = runServer({
        isDev,
        handle,
    });

    // RUN SERVER
    server.listen(port, err => {
        if (err) throw err;

        // eslint-disable-next-line no-console
        console.log(`> Ready on http://localhost:${port}`);
    });
});
