const path = require('path');
const express = require('express');
const expressGraphQL = require('express-graphql');
const morgan = require('morgan');

const handleGlobalError = require('./middlewares/handleGlobalError');
const tameMongoErrors = require('./middlewares/tameMongoErrors');
const schema = require('./schema');

const run = ({ isDev, handle }) => {
    const server = express();

    // SERVE STATIC
    server.use('/static', express.static(path.join(__dirname, '..', 'static')));

    if (isDev) {
        server.use(morgan(isDev ? 'dev' : 'common'));
    }

    // API
    server.use(
        '/graphql',
        expressGraphQL({
            schema,
            graphiql: true,
        }),
    );
    server.get('*', handle);

    // HANDLE ERRORS
    server.use(tameMongoErrors, handleGlobalError);

    return server;
};

module.exports = run;
