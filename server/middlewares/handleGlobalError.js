const handleGlobalError = (err, req, res, next) => {
    const statusCode = err.statusCode || 500;
    const status = err.status || 'error';

    if (process.env.NODE_ENV === 'development') {
        return res.status(statusCode).json({
            status,
            error: err,
            message: err.message,
        });
    }

    if (err.isOperational) {
        return res.status(statusCode).json({
            status,
            message: err.message,
        });
    }

    // TODO: replace with morgan
    // eslint-disable-next-line no-console
    console.log(err);

    return res.status(500).json({
        status: 'error',
        message: 'Something went wrong',
    });
};

module.exports = handleGlobalError;
