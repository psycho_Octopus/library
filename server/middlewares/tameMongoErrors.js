const R = require('ramda');

const GlobalError = require('../utils/errors/GlobalError');

const tameMongoErrors = (err, req, res, next) => {
    // eslint-disable-next-line default-case
    switch (err.name) {
        case 'CastError': {
            const error = new GlobalError(`Invalid ${err.path}: ${err.value}`, 404);

            return next(error);
        }

        case 'ValidationError': {
            const formatError = R.compose(
                R.join(' '),
                R.values,
                R.pick(['path', 'value']),
            );
            const errorsFlow = R.compose(
                R.join(', '),
                R.map(formatError),
                R.values,
            );

            const error = new GlobalError(`Invalid params: ${errorsFlow(err.errors)}`, 401);

            return next(error);
        }

        case 'MongoError': {
            // eslint-disable-next-line default-case
            switch (err.code) {
                case 11000: {
                    const error = new GlobalError(`Duplicate field`, 400);

                    return next(error);
                }
            }

            break;
        }

        // eslint-disable-next-line no-fallthrough
        case 'JsonWebTokenError': {
            const error = new GlobalError('Invalid token', 401);

            return next(error);
        }

        case 'TokenExpiredError': {
            const error = new GlobalError('Token expired', 401);

            return next(error);
        }
    }

    return next(err);
};

module.exports = tameMongoErrors;
