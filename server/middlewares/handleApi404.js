const GlobalError = require('../utils/errors/GlobalError');

const handleApi404 = (req, res, next) => {
    const err = new GlobalError(`Can't find ${req.originalUrl} on this server`, 404);

    next(err);
};

module.exports = handleApi404;
