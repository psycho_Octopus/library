class GlobalError extends Error {
    constructor(message, statusCode) {
        super(message);

        this.statusCode = statusCode;
        this.status = this.statusCode >= 500 ? 'error' : 'fail';
        this.isOperational = true;
    }
}

module.exports = GlobalError;
