const GlobalError = require('./GlobalError');

class AuthError extends GlobalError {
    constructor() {
        super('User not found', 404);
    }
}

module.exports = AuthError;
