const GlobalError = require('./GlobalError');

class AuthError extends GlobalError {
    constructor(statusCode = 400) {
        super('Invalid email or password', statusCode);
    }
}

module.exports = AuthError;
