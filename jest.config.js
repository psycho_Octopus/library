module.exports = {
    testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
    setupFilesAfterEnv: ['jest-extended'],
    transform: {
        '^.+\\.jsx?$': 'babel-jest',
    },
};
