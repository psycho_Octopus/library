export type Response<D> = {
    status: 'success' | 'fail' | 'error';
    data: D;
};
