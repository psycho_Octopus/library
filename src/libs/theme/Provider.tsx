import React, { useReducer, ReactNode } from "react";

import { theme } from "./types";
import ThemeContext from "./context";

type State = {
  theme: theme;
};

type Action = { type: "SET_THEME"; theme: theme };

const setTheme = (newTheme: theme): Action => ({
  type: "SET_THEME",
  theme: newTheme,
});

const initialState: State = {
  theme: "light",
};

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case "SET_THEME":
      return {
        ...state,
        theme: action.theme,
      };
    default:
      return state;
  }
};

type ProviderProps = {
  children: ReactNode;
};

const ThemeProvider = ({ children }: ProviderProps) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const { Provider } = ThemeContext;

  return (
    <Provider
      value={{
        theme: state.theme,
        setTheme: (newTheme: theme) => {
          dispatch(setTheme(newTheme));
        },
      }}
    >
      {children}
    </Provider>
  );
};

export default ThemeProvider;
