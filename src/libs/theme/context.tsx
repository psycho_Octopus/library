import { createContext } from "react";

import { theme } from "./types";

type Values = {
  theme: theme;
  setTheme: (theme: theme) => void;
};

const initialValues: Values = {
  theme: "light",
  setTheme: () => {},
};

const ThemeContext = createContext(initialValues);

export default ThemeContext;
