import { schema } from 'normalizr';

import { Book } from '../../../types/entities';

const booksSchema = new schema.Entity<Book>(
    'books',
    {},
    {
        idAttribute: '_id',
        processStrategy: entity => ({
            id: entity._id,
            ...entity,
        }),
    },
);

export default booksSchema;
