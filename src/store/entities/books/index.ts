import * as actions from './actions';
import * as selectors from './selectors';
import schema from './schema';
import reducer, { LocalState } from './reducer';
import { key } from './constants';

export type BooksState = LocalState;

export default {
    ...actions,
    ...selectors,
    schema,
    reducer,
    key,
};
