import { makeReducer } from '../../../utils/redux';

export type LocalState = {};

const defaultState: LocalState = {};

const booksReducer = makeReducer<LocalState, any>(defaultState, {});

export default booksReducer;
