import { makeActionTypes } from '../../../utils/redux';

import { key } from './constants';

export default makeActionTypes(key, [], ['FETCH']);
