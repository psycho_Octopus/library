import { compose, uncurryN, propOr, pathOr, identity, map, applySpec } from 'ramda';

import { Book, Tag } from '../../../types/entities';
import { Selector, Entities } from '../../../types/redux';

import { key } from './constants';

/**
 * Get book entities object
 *
 * @param {Object} state App state
 */
export const get: Selector<Entities<Book>> = pathOr({}, ['entities', key]);

/**
 * Get one book by its id
 *
 * @param {string|number} bookId
 * @param {Object} state App state
 */
export const getById: (id: Book['id']) => Selector<Book> = uncurryN(2, (bookId: Book['id']) =>
    compose(
        propOr({}, bookId),
        get,
    ),
);

/**
 * Get TagsList compatible tags
 *
 * @param {Object} state App state
 */
export const getTagsFormatted: (id: Book['id']) => Selector<Tag[]> = uncurryN(
    2,
    (bookId: Book['id']) =>
        compose(
            map(
                applySpec({
                    id: identity,
                    title: identity,
                }),
            ),
            propOr([], 'tags'),
            propOr({}, bookId),
            get,
        ),
);
