import getConfig from 'next/config';
import axios from 'axios';
import { normalize } from 'normalizr';
import { ThunkAction } from 'redux-thunk';

import { Book } from '../../../types/entities';
import { NormalizedEntities, createAction, Error } from '../../../types/redux';

import actionTypes from './actionTypes';
import schema from './schema';

export type FetchSuccessAction = ReturnType<typeof fetchSuccess>;
export type FetchFailureAction = ReturnType<typeof fetchFailure>;

const { publicRuntimeConfig } = getConfig();

const fetchSuccess = (params: { entities: NormalizedEntities<Book> }) =>
    createAction(actionTypes.FETCH_SUCCESS, params);

const fetchFailure = (error: Error) => createAction(actionTypes.FETCH_FAILURE, { error });

export const fetch = (
    id: Book['id'],
): ThunkAction<
    Promise<Book>,
    any,
    null,
    FetchSuccessAction | FetchFailureAction
> => async dispatch => {
    try {
        const {
            data: { data: book },
        } = await axios(`${publicRuntimeConfig.hostName}/api/v1/books/${id}`);
        const normalizedBook = normalize(book, schema);
        const successAction = fetchSuccess({
            ...normalizedBook,
        });

        dispatch(successAction);

        return book;
    } catch (e) {
        const failureAction = fetchFailure({
            reason: 'fetch',
        });

        dispatch(failureAction);

        return [];
    }
};
