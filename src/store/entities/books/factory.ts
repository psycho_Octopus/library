import { createEntityFactory } from '../../../utils/redux';
import schema from './schema';

export default createEntityFactory(
    {},
    {
        schema,
        schemas: [schema],
    },
);
