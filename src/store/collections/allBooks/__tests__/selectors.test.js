import bookEntity from '../../../entities/books';
import bookEntityFactory from '../../../entities/books/factory';
import { key } from '../constants';
import { getData, hasBooksFound, hasMoreBooks, getMeta } from '../selectors';
import { defaultState } from '../reducer';

jest.mock('../../../entities/books', () => ({
    get: jest.fn(),
}));

const createState = (state = {}) => ({
    collections: {
        [key]: {
            ...defaultState,
            ...state,
        },
    },
});

const book1 = bookEntityFactory({
    title: 'Book 1',
});
const book2 = bookEntityFactory({
    title: 'Book 2',
});
const book3 = bookEntityFactory({
    title: 'Book 3',
});

describe('getData', () => {
    beforeAll(() => {
        bookEntity.get.mockImplementation(() => ({
            [book1.id]: book1,
            [book2.id]: book2,
            [book3.id]: book3,
        }));
    });

    afterAll(() => {
        bookEntity.get.resetMock();
    });

    it('returns data', () => {
        [
            { data: [book1.id], expected: [book1] },
            { data: [book1.id, book2.id], expected: [book1, book2] },
            { data: [book1.id, book2.id, 666], expected: [book1, book2] },
        ].forEach(({ data, expected }) => {
            const state = createState({ data });

            expect(getData(state)).toEqual(expected);
        });
    });

    it('returns an empty array if there is no data', () => {
        const state = createState({
            data: [],
        });

        expect(getData(state)).toBeEmpty();
    });

    it('returns an empty array if there is no required entities', () => {
        const state = createState({
            data: [666], // book with id 666 doesn't exist
        });

        expect(getData(state)).toBeEmpty();
    });
});

describe('getMeta', () => {
    it('returns meta', () => {
        const meta = {
            error: null,
            totalCount: 10,
            status: 'idle',
        };
        const state = createState(meta);

        expect(getMeta(state)).toEqual(meta);
    });
});

describe('hasBooksFound', () => {
    it('returns "true" is there are any books', () => {
        const state = createState({
            data: [book1.id],
        });

        expect(hasBooksFound(state)).toBeTrue();
    });

    it('returns "false" is there are no books', () => {
        const state = createState({
            data: [],
        });

        expect(hasBooksFound(state)).toBeFalse();
    });
});

describe('hasMoreBooks', () => {
    it('returns "true" is there are any more books', () => {
        const state = createState({
            data: [book1.id],
            totalCount: 2,
        });

        expect(hasMoreBooks(state)).toBeTrue();
    });

    it('returns "false" is there are no more books', () => {
        const state = createState({
            data: [book1.id],
            totalCount: 1,
        });

        expect(hasMoreBooks(state)).toBeFalse();
    });
});
