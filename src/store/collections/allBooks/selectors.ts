import { compose, converge, prop, pathOr, pick, values, gt, length, __ } from 'ramda';

import { Book } from '../../../types/entities';
import { Selector, Collection, CollectionMeta, CollectionData } from '../../../types/redux';
import books from '../../entities/books';

import { key } from './constants';

const get: Selector<Collection<Book['id']>> = pathOr([], ['collections', key]);

const data: Selector<Book['id'][]> = compose(
    prop('data'),
    get,
);

const totalCount: Selector<number> = compose(
    prop('totalCount'),
    get,
);

// TODO: Описать entity
/**
 * @template E entity object
 */
const collectEntities = <E>(entity: any): Selector<CollectionData<E>> =>
    compose(
        values,
        converge(pick, [data, entity.get]),
    );

export const getData = collectEntities<Book>(books);

export const getMeta: Selector<CollectionMeta<Book['id']>> = compose(
    pick(['status', 'error', 'totalCount', 'data']),
    get,
);

export const hasBooksFound: Selector<boolean> = compose(
    gt(__, 0),
    length,
    getData,
);

export const hasMoreBooks: Selector<boolean> = converge(gt, [
    totalCount,
    compose(
        length,
        getData,
    ),
]);
