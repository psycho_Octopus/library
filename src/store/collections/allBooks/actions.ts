import getConfig from 'next/config';
import axios from 'axios';
import { normalize } from 'normalizr';
import queryString from 'query-string';
import { ThunkAction } from 'redux-thunk';

import { Response } from '../../../../shared/types';
import { Book } from '../../../types/entities';
import { Error, createAction, NormalizedEntities, NormalizedResult } from '../../../types/redux';
import appConfig from '../../../appConfig';
import booksEntity from '../../entities/books';

import actionTypes from './actionTypes';

const { publicRuntimeConfig } = getConfig();

export type FetchAction = ReturnType<typeof fetch>;
export type FetchSuccessAction = ReturnType<typeof fetchSuccess>;
export type FetchFailureAction = ReturnType<typeof fetchFailure>;

const fetchSuccess = (params: {
    entities: NormalizedEntities<Book>;
    result: NormalizedResult<Book['id']>;
    totalCount: number;
    append: boolean;
}) => createAction(actionTypes.FETCH_SUCCESS, params);

const fetchFailure = (error: Error) => createAction(actionTypes.FETCH_FAILURE, { error });

type FetchParams = {
    title: string;
    location?: string; // TODO: Заменить на LocationsEnum
    offset: number;
    tags: string[];
    append?: boolean;
};

export const fetch = ({
    title,
    location = appConfig.features.filter.defaultLocation,
    offset,
    tags,
    append = false,
}: FetchParams): ThunkAction<
    Promise<Book[]>,
    any,
    null,
    FetchSuccessAction | FetchFailureAction
> => async dispatch => {
    const query = queryString.stringify({ title, location, offset, tags, limit: 4 });

    try {
        const [
            {
                data: { data: books },
            },
            {
                data: { data: count },
            },
        ] = await Promise.all([
            axios.get<Response<Book[]>>(`${publicRuntimeConfig.hostName}/api/v1/books?${query}`),
            axios.get<Response<number>>(`${publicRuntimeConfig.hostName}/api/v1/books/count`),
        ]);

        const { entities, result } = normalize<
            Book,
            FetchSuccessAction['payload']['entities'],
            FetchSuccessAction['payload']['result']
        >(books, [booksEntity.schema]);
        const successAction = fetchSuccess({ entities, result, totalCount: count, append });

        dispatch(successAction);

        return books;
    } catch (error) {
        const failureAction = fetchFailure({
            reason: 'fetch',
        });

        dispatch(failureAction);

        return [];
    }
};
