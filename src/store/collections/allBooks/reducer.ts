import { compose, mergeLeft, assoc } from 'ramda';

import { Book } from '../../../types/entities';
import { Error, StateFlow } from '../../../types/redux';
import { makeReducer } from '../../../utils/redux';

import actionTypes from './actionTypes';
import { FetchSuccessAction, FetchFailureAction } from './actions';

export type LocalState = {
    data: Book['id'][];
    status: 'idle';
    totalCount: number;
    error: Error | null;
};
type Actions = FetchSuccessAction | FetchFailureAction;

export const defaultState: LocalState = {
    data: [],
    status: 'idle',
    totalCount: 0,
    error: null,
};

export default makeReducer<LocalState, Actions>(defaultState, {
    [actionTypes.FETCH_SUCCESS](state, action): LocalState {
        const { payload } = action as FetchSuccessAction;
        const flow: StateFlow<LocalState> = mergeLeft({
            status: 'idle',
            totalCount: payload.totalCount,
            data: payload.append ? [...state.data, ...payload.result] : payload.result,
        });

        return flow(state);
    },

    [actionTypes.FETCH_FAILURE](state, action: FetchFailureAction): LocalState {
        const { payload } = action as FetchFailureAction;
        const flow: StateFlow<LocalState> = compose(
            assoc('status', 'idle'),
            assoc('error', payload.error),
        );

        return flow(state);
    },
});
