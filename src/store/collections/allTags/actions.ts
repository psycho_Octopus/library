import getConfig from 'next/config';
import axios from 'axios';
import queryString from 'query-string';
import { ThunkAction } from 'redux-thunk';

import { LocationEnum } from '../../../enums';
import { Response } from '../../../../shared/types';
import { Error, createAction } from '../../../types/redux';
import { Tag } from '../../../types/entities';

import actionTypes from './actionTypes';

const { publicRuntimeConfig } = getConfig();

export type FetchAction = ReturnType<typeof fetch>;
export type FetchSuccessAction = ReturnType<typeof fetchSuccess>;
export type FetchFailureAction = ReturnType<typeof fetchFailure>;

export const fetchSuccess = (params: { tags: Tag[] }) =>
    createAction(actionTypes.FETCH_SUCCESS, params);

const fetchFailure = (error: Error) => createAction(actionTypes.FETCH_FAILURE, { error });

export const fetch = ({
    location,
}: {
    location: LocationEnum;
}): ThunkAction<
    Promise<Tag[]>,
    any,
    null,
    FetchSuccessAction | FetchFailureAction
> => async dispatch => {
    const query = queryString.stringify({ location });

    try {
        const {
            data: { data: tags },
        } = await axios.get<Response<Tag[]>>(
            `${publicRuntimeConfig.hostName}/api/v1/books/tags?${query}`,
        );
        const successAction = fetchSuccess({ tags });

        dispatch(successAction);

        return tags;
    } catch (e) {
        const failureAction = fetchFailure({
            reason: 'fetch',
        });

        dispatch(failureAction);

        return [];
    }
};
