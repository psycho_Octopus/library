import { compose, prop, pathOr, pick, map, applySpec, join, values, reverse } from 'ramda';

import { Tag } from '../../../types/entities';
import { Selector, Collection, CollectionMeta } from '../../../types/redux';

import { key } from './constants';

const get: Selector<Collection<Tag>> = pathOr([], ['collections', key]);

export const getData: Selector<Tag[]> = compose(
    prop('data'),
    get,
);

export const getMeta: Selector<CollectionMeta<Tag>> = compose(
    pick(['status', 'error']),
    get,
);

type TagFormatted = {
    id: string;
    title: string;
};

export const getDataFormatted: Selector<TagFormatted[]> = compose(
    map(
        applySpec({
            id: prop('name'),
            title: compose(
                join(' '),
                reverse,
                values,
            ),
        }),
    ),
    getData,
);
