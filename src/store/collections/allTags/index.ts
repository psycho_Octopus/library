import * as actions from './actions';
import * as selectors from './selectors';
import reducer, { LocalState } from './reducer';
import { key } from './constants';

export type AllTagsState = LocalState;

export default {
    ...actions,
    ...selectors,
    reducer,
    key,
};
