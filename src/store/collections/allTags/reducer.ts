import { compose, assoc } from 'ramda';

import { Error, StateFlow } from '../../../types/redux';
import { makeReducer } from '../../../utils/redux';

import actionTypes from './actionTypes';
import { FetchSuccessAction, FetchFailureAction } from './actions';

export type LocalState = {
    data: string[];
    status: 'idle';
    error: Error | null;
};
type Actions = FetchSuccessAction | FetchFailureAction;

const defaultState: LocalState = {
    data: [],
    status: 'idle',
    error: null,
};

export default makeReducer<LocalState, Actions>(defaultState, {
    [actionTypes.FETCH_SUCCESS](state, action): LocalState {
        const { payload } = action as FetchSuccessAction;

        const flow: StateFlow<LocalState> = compose(
            assoc('status', 'idle'),
            assoc('data', payload.tags),
        );

        return flow(state);
    },

    [actionTypes.FETCH_FAILURE](state, action) {
        const { payload } = action as FetchFailureAction;

        const flow: StateFlow<LocalState> = compose(
            assoc('status', 'idle'),
            assoc('error', payload.error),
        );

        return flow(state);
    },
});
