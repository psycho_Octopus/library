import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { combineEntities } from '../utils/redux';

// Collections
import allBooks, { AllBooksState } from './collections/allBooks';
import allTags, { AllTagsState } from './collections/allTags';
// Entities
import books, { BooksState } from './entities/books';

export type AppState = {
    collections: {
        allBooks: AllBooksState;
        allTags: AllTagsState;
    };
    entities: {
        books: BooksState;
    };
};

const reducer = combineReducers<AppState>({
    collections: combineReducers({
        allBooks: allBooks.reducer,
        allTags: allTags.reducer,
    }),
    entities: combineEntities({
        books: books.reducer,
    }),
});

export const create = (initialState: Partial<AppState>) => {
    const store = createStore<AppState, any, unknown, unknown>(
        reducer,
        initialState,
        composeWithDevTools(applyMiddleware(thunk)),
    );

    return store;
};
