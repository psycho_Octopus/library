export const FieldErrors = {
  Required: "Required",
  MaxLength: "MaxLength",
};

type ValidatorType = (value: string) => string | undefined;
type ValidatorWithSettingsType = (...args: any[]) => ValidatorType;

export const isRequired: ValidatorType = value =>
  value.length === 0 ? FieldErrors.Required : undefined;

export const maxLength: ValidatorWithSettingsType = (n: number) => (value = "") =>
  value.length > n ? FieldErrors.MaxLength : undefined;
