export const makeTitle = (title: string): string => `${title} | Library`;
