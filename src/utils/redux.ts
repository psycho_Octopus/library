/* eslint-disable no-param-reassign */
import { combineReducers, ReducersMapObject } from 'redux';
import faker from 'faker';
import { normalize } from 'normalizr';
// TODO: Remove lodash and use ramda
import mergeWith from 'lodash/mergeWith';

import { Action, ActionHandlersMap } from '../types/redux';

export const makeActionTypes = (
    namespace: string = '',
    syncTypes: string[] = [],
    asyncTypes: string[] = [],
) => {
    const syncActionTypes = syncTypes.reduce<Record<string, string>>((accamulator, item) => {
        accamulator[item] = `${namespace}/${item}`;

        return accamulator;
    }, {});

    const asyncActionTypes = asyncTypes.reduce<Record<string, string>>((accamulator, item) => {
        accamulator[item] = `${namespace}/${item}`;
        accamulator[`${item}_REQUEST`] = `${namespace}/${item}_REQUEST`;
        accamulator[`${item}_FAILURE`] = `${namespace}/${item}_FAILURE`;
        accamulator[`${item}_SUCCESS`] = `${namespace}/${item}_SUCCESS`;

        return accamulator;
    }, {});

    return {
        ...syncActionTypes,
        ...asyncActionTypes,
    };
};

export const makeActionType = (namespace: string, actionTypeName: string) =>
    `${namespace}/${actionTypeName}`;

export const makeActionTypeName = (name: string) => name.replace(/([A-Z])/g, '_$1').toUpperCase();

export const makeActionNameFromActionTypeName = (name: string) => {
    return name.toLowerCase().replace(/(_([a-z]))/g, (match, x1, x2) => {
        return x2.toUpperCase();
    });
};

/**
 * @param {Object} defaultState default state
 * @param {Object} handlers map of all action handler
 *
 * @template S state
 * @template A union of all possible action
 *
 * @returns {function} action handler
 */
export const makeReducer = <S, A extends Action>(
    defaultState: S,
    handlers: ActionHandlersMap<S, A> = {},
) => (state: S = defaultState, action: A) => {
    if (!Object.prototype.hasOwnProperty.call(handlers, action.type)) {
        return state;
    }

    const currHandler = handlers[action.type];

    // if (Array.isArray(currHandler)) {
    //     return currHandler.reduce<S>((nextState, handler) => handler(nextState, action), state);
    // }

    return currHandler(state, action);
};

export const combineEntities = <S>(entities: ReducersMapObject<S, any>) => {
    const entitiesReducer = combineReducers<S>(entities);

    return (state: any, action: any) => {
        let nextState;

        const newEntities = action.payload ? action.payload.entities : undefined;

        if (newEntities) {
            nextState = Object.keys(newEntities).reduce((acc, key) => {
                acc[key] = mergeWith({}, state[key], newEntities[key], (oldValue, newValue) => {
                    if (Array.isArray(oldValue)) {
                        return newValue;
                    }

                    if (
                        typeof oldValue === 'object' &&
                        Object.keys(oldValue) !== Object.keys(newValue)
                    ) {
                        return newValue;
                    }

                    return undefined;
                });

                return acc;
            }, state);
        } else {
            nextState = state;
        }

        return entitiesReducer(nextState, action);
    };
};

export const createEntityFactory = (defaults = {}, { schema, schemas, idType = 'number' }: any) => (
    data = {},
    wrap = false,
) => {
    const key = schema._key; // eslint-disable-line
    const ID_TYPE_MAP = {
        number: 'number',
        string: 'uuid',
        hash: 'uuid',
    };
    // @ts-ignore
    const genId = typeof idType === 'function' ? idType : faker.random[ID_TYPE_MAP[idType]];
    let normalized;

    if (Array.isArray(data)) {
        const actualData = data.length === 0 ? [{}] : data; // eslint-disable-line
        const raw = actualData.map(item => {
            const id = item.id || genId(faker);

            return {
                id,
                [schema._idAttribute]: id, // eslint-disable-line
                ...defaults,
                ...item,
            };
        });

        normalized = normalize(raw, schemas);

        if (wrap) {
            // @ts-ignore
            normalized.raw = raw;

            return normalized;
        }

        return Object.values(normalized.entities[key]);
    }

    // @ts-ignore
    const id = data.id || genId(faker);
    const raw = {
        id,
        [schema._idAttribute]: id, // eslint-disable-line
        ...defaults,
        ...data,
    };

    normalized = normalize(raw, schema);

    if (wrap) {
        // @ts-ignore
        normalized.raw = raw;

        return normalized;
    }

    return normalized.entities[key][id];
};
