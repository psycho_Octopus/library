import { ApolloClient, HttpLink, InMemoryCache } from '@apollo/client';
import fetch from 'cross-fetch';

const createApolloClient = (initialState: any, ctx: any) => {
    const client = new ApolloClient({
        link: new HttpLink({
            uri: 'http://localhost:3000/graphql',
            fetch,
        }),
        cache: new InMemoryCache().restore(initialState),
        ssrMode: Boolean(ctx),
        ssrForceFetchDelay: 500,
    });

    return client;
};

export default createApolloClient;
