const withCSS = require('@zeit/next-css');
const withSass = require('@zeit/next-sass');
const withImages = require('next-images');

require('dotenv').config();

module.exports = withCSS(
    withImages(
        withSass({
            distDir: '../.next',
            publicRuntimeConfig: {
                hostName: process.env.HOST_NAME,
            },
        }),
    ),
);
