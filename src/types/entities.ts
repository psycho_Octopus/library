import { LocationEnum } from "../enums";

/**
 * TAG
 */
export type Tag = {
  id: string;
  title: string;
};

/**
 * BOOK
 */
export enum BookStatus {
  Waited = "WAITED",
  Available = "AVAILABLE",
  Occupied = "OCCUPIED",
}

export type Book = {
  id: number;
  status: BookStatus;
  title: string;
  slug: string;
  description: string;
  fullDescription: string;
  cover: string;
  location: LocationEnum;
  tags: Tag[];
  ratingAverage: number;
  ratingQuantity: number;
  takenBy: User;
};

/**
 * USER
 */
export type User = {
  id: number;
  name: string;
  avatar: string;
};
