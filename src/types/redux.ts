import { Book, User } from './entities';
import { AppState } from '../store';

/**
 * @template I id type
 */
export type Collection<I> = {
  /**
   * Array of ids
   */
  data: I[];
  /**
   * Status of collection.
   * Default value: idle
   * Example: fetch, idle
   */
  status: string;
  /**
   * Error object
   */
  error?: any;
  /**
   * Total count of items in collection
   */
  totalCount?: number;
};

/**
 * Shape of meta object
 *
 * @template I id type
 */
export type CollectionMeta<I> = Pick<Collection<I>, 'data' | 'status' | 'error' | 'totalCount'>;

/**
 * @template E entity type
 */
export type CollectionData<E> = E[];

export type Entities<E> = {
  [id: string]: E;
  [id: number]: E;
};

export type Error = {
  reason: string;
};

export type Action = {
  type: string;
};

export type ActionWithPayload<P> = Action & {
  payload: P;
};

export function createAction(type: string): Action;
export function createAction<P>(type: string, payload: P): ActionWithPayload<P>;
export function createAction<P>(type: string, payload?: P) {
  return payload === undefined ? { type } : { type, payload };
}

/**
 * @template I id type
 */
export type NormalizedEntities<I extends Book | User> = {
  [entityName: string]: {
    [id: string]: I;
  };
};

export type NormalizedResult<T extends Book['id'] | User['id']> = T[];

export interface ActionHandler<S, A> {
  (state: S, action: A): S;
}

export type ActionHandlersMap<S, A> = {
  [actionType: string]: ActionHandler<S, A>;
};

/**
 * @template R return value
 */
export interface Selector<R> {
  (State: AppState): R;
}

export interface StateFlow<S> {
  (state: S): S;
}
