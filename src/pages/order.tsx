import React from 'react';
import Head from 'next/head';

import DocTitle from '../components/base/DocTitle';
import { withApollo } from '../libs/next/withApollo';
import OrderBookModule from '../components/modules/OrderBook';

const OrderPage = () => (
  <>
    <Head>
      <DocTitle>Order</DocTitle>
    </Head>
    <OrderBookModule />
  </>
);

export default withApollo({ ssr: true })(OrderPage);
