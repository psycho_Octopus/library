import React from 'react';
import Head from 'next/head';

import DocTitle from '../components/base/DocTitle';
import { withApollo } from '../libs/next/withApollo';
import LoginModule from '../components/modules/Login';

const LoginPage = () => (
  <>
    <Head>
      <DocTitle>Login</DocTitle>
    </Head>
    <LoginModule />
  </>
);

export default withApollo({ ssr: true })(LoginPage);
