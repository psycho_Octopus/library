import React from 'react';
import Head from 'next/head';

import { withApollo } from '../libs/next/withApollo';
import DocTitle from '../components/base/DocTitle';
import FilterForm from '../components/domains/forms/Filter';
import AllBooksListModule from '../components/modules/AllBooksList';
import WithSearchLayout from '../components/layouts/WithSearch';

const BooksPage = () => {
  return (
    <>
      <Head>
        <DocTitle>All books</DocTitle>
      </Head>
      <WithSearchLayout>
        {/* FILDER FORM */}
        <WithSearchLayout.Search>
          <FilterForm />
        </WithSearchLayout.Search>

        {/* ALL BOOKS LIST MODULE */}
        <WithSearchLayout.Main>
          <AllBooksListModule />
        </WithSearchLayout.Main>
      </WithSearchLayout>
    </>
  );
};

export default withApollo({ ssr: true })(BooksPage);
