import React from 'react';
import { string } from 'prop-types';
import { NextPageContext } from 'next';
import Head from 'next/head';

import { Book } from '../../types/entities';
import { withApollo } from '../../libs/next/withApollo';
import DocTitle from '../../components/base/DocTitle';
import WithSearchLayout from '../../components/layouts/WithSearch';
import FilterForm from '../../components/domains/forms/Filter';
import BookDetailsModule from '../../components/modules/BookDetails';

type Props = {
  bookId: Book['id'];
};
const BookPage = ({ bookId }: Props) => {
  return (
    <>
      <Head>
        {/* TODO: Add book name */}
        <DocTitle>Book</DocTitle>
      </Head>

      <WithSearchLayout>
        <WithSearchLayout.Search>
          {/* FILDER FORM */}
          <FilterForm />
        </WithSearchLayout.Search>
        <WithSearchLayout.Main>
          {/* BOOK DETAIL MODULE */}
          <BookDetailsModule bookId={bookId} />
        </WithSearchLayout.Main>
      </WithSearchLayout>
    </>
  );
};

BookPage.getInitialProps = (ctx: NextPageContext) => {
  return {
    bookId: ctx.query.id,
  };
};

BookPage.propTypes = {
  bookId: string.isRequired,
};

export default withApollo({ ssr: true })(BookPage);
