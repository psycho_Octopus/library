import React from "react";
import App from "next/app";
import Head from "next/head";
import Router from "next/router";
import NProgress from "nprogress";

import { ThemeProvider } from "../libs/theme";
import ShellLayout from "../components/layouts/Shell";
import "nprogress/nprogress.css";
import "../styles/base.scss";

Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

class LibraryApp extends App {
  render() {
    const { Component, pageProps } = this.props;

    return (
      <ThemeProvider>
        <ShellLayout>
          <Head>
            <link
              href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&display=swap"
              rel="stylesheet"
            />
            <link
              rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossOrigin="anonymous"
            />
          </Head>
          <Component {...pageProps} />
        </ShellLayout>
      </ThemeProvider>
    );
  }
}

export default LibraryApp;
