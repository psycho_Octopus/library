import LocationsEnum from '../shared/enums/locations';

export default {
  features: {
    filter: {
      defaultLocation: LocationsEnum.Spb,
    },
  },
};
