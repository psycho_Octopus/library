import React, { useContext } from "react";
import Navbar from "react-bootstrap/Navbar";
import bemCl from "bem-cl";

// import AppLogo from '../../domains/AppLogo/AppLogo';
import Nav from "../../domains/Nav";

import "./Header.scss";

export const b = bemCl("header");

const HeaderModule = () => {
  return (
    <header className={b()}>
      <Navbar bg="dark" variant="dark">
        {/* <AppLogo /> */}
        <Nav />
      </Navbar>
    </header>
  );
};

export default HeaderModule;
