import React from 'react';
import { useQuery } from '@apollo/client';

import { Book } from '../../../types/entities';
import Title from '../../base/Title';
import BookDetails from '../../domains/BookDetails';

import BOOK_DETAILS_QUERY from './query.js';

type Props = {
  bookId: Book['id'];
};

const BookDetailsModule = ({ bookId }: Props) => {
  const { data, error } = useQuery(BOOK_DETAILS_QUERY, {
    variables: {
      id: bookId,
    },
  });

  if (error) {
    return <Title>Ooops...</Title>;
  }

  if (!data) {
    return <Title>Loading...</Title>;
  }

  if (!data.book) {
    return <Title>Book not found</Title>;
  }

  return <h1>Book detail</h1>;
};

export default BookDetailsModule;
