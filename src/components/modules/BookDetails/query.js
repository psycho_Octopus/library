import { gql } from '@apollo/client';

const QUERY = gql`
  query bookDetails($id: Int) {
    book(id: $id) {
      id
      title
      cover
      description
      ratingAverage
    }
  }
`;

export default QUERY;
