import { gql } from "@apollo/client";

const QUERY = gql`
  query allBooks($title: String) {
    books(title: $title) {
      id
      status
      title
      description
      fullDescription
      cover
      location
      ratingAverage
      ratingQuantity
      takenBy {
        id
        name
        avatar
      }
    }
  }
`;

export default QUERY;
