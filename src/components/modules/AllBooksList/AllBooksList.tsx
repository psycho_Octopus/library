import React from "react";
import { useQuery } from "@apollo/client";
import { useRouter } from "next/router";

import Title from "../../base/Title";
import BooksList from "../../domains/BooksList";

import ALL_BOOKS_QUERY from "./query";

const AllBooksListModule = () => {
  const router = useRouter();
  const { data, error } = useQuery(ALL_BOOKS_QUERY, {
    variables: {
      title: router.query.title,
    },
  });

  if (error) {
    return <Title>Ooops...</Title>;
  }

  if (!data) {
    return <Title>Loading...</Title>;
  }

  if (data.books.length === 0) {
    return <Title>No books found</Title>;
  }

  return (
    <>
      <Title>Books found: 100</Title>
      <BooksList items={data.books} />
    </>
  );
};

export default AllBooksListModule;
