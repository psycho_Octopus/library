import React from 'react';
import bemCl from 'bem-cl';

import { Tag, Book } from '../../../types/entities';
import Rating, { RatingSizes } from '../../base/Rating';
import Title from '../../base/Title';
// import TagsList from '../../base/TagsList';

import './BookDetails.scss';

type TagId = Tag['id'];
type Props = {
  data: Book;
};

const b = bemCl('book-details');

const BookDetails = ({ data }: Props) => {
  // const tags = [];

  // const handleTagsSelect = (selectedTags: TagId[]) => {
  //     router.push({
  //         pathname: '/books',
  //         query: {
  //             location,
  //             tags: selectedTags,
  //         },
  //     });
  // };

  const { title, cover, description, ratingAverage } = data;

  return (
    <article className={b()}>
      <div className={b('cover')}>
        <img src={cover} alt="" />
      </div>
      <div className={b('main')}>
        <Title>{title}</Title>
        {ratingAverage > 0 && <Rating value={ratingAverage} size={RatingSizes.L} />}
        <p>{description}</p>
        {/* <TagsList
            className={b('tags').toString()}
            items={tags}
            onSelect={handleTagsSelect}
        /> */}
      </div>
    </article>
  );
};

export default BookDetails;
