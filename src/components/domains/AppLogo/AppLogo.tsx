import React from 'react';
import Link from 'next/link';
import bemCl from 'bem-cl';

// import src from './images/logo.jpg';
import './AppLogo.scss';

const b = bemCl('app-logo');

const AppLogo = () => {
  return (
    <Link href="/">
      {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
      <a className={b()}>{/* <img src={src} alt="Logo" /> */}</a>
    </Link>
  );
};

export default AppLogo;
