import React, { ReactNode } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import bemCl from "bem-cl";
import Button from "react-bootstrap/Button";

import { Tag, Book, BookStatus } from "../../../types/entities";
import Rating from "../../base/Rating";
import TagsList from "../../base/TagsList";

import "./BookPreview.scss";

type Props = {
  children: ReactNode;
  className: string;
  data: Book;
};

const b = bemCl("book-preview");

const BookPreview = ({ className = "", data }: Props) => {
  const router = useRouter();
  const { id, title, cover, description, ratingAverage, status, takenBy } = data;

  const handleTagsSelect = (selectedTags: Tag["id"][]) => {
    router.push({
      pathname: "/books",
      query: {
        ...router.query,
        tags: selectedTags,
      },
    });
  };

  return (
    <div className={b().mix(className)}>
      <div className={b("aside")}>
        <Link href="/books/[id]" as={`/books/${id}`}>
          <img className={b("cover-img")} src={cover} alt="" />
        </Link>
        <div className={b("actions")}>
          {status === BookStatus.Available ? (
            <Button type="button" size="sm" variant="success" block>
              Take
            </Button>
          ) : null}
        </div>
      </div>
      <div className={b("main")}>
        <h2 className={b("title").toString()}>
          <Link href="/books/[id]" as={`/books/${id}`}>
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a className={b("title-link")}>{title}</a>
          </Link>
        </h2>
        {ratingAverage > 0 && <Rating value={ratingAverage} />}
        <p className={b("description").toString()}>{description}</p>
        {/* <TagsList
            className={b('tags').toString()}
            items={tags}
            onSelect={handleTagsSelect}
        /> */}
      </div>
    </div>
  );
};

export default BookPreview;
