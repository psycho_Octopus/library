import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useInView } from 'react-intersection-observer';
import bemCl from 'bem-cl';

import { Book } from '../../../types/entities';
import BookPreview from '../BookPreview';

import './BooksList.scss';

const b = bemCl('books-list');

type Props = {
  items: [Book];
};

const BooksList = ({ items }: Props) => {
  const router = useRouter();
  const allBooksMeta = { data: [] };
  const hasMoreBooks = false;
  const [ref, inView] = useInView();

  useEffect(() => {
    if (hasMoreBooks && inView) {
      console.log('load more books');
    }
  }, [inView]);

  return (
    <div className={b()}>
      {/* BOOKS */}
      {items.map(book => (
        <BookPreview className={b('item').toString()} data={book} key={book.id}>
          <button type="button">awdaw</button>
        </BookPreview>
      ))}

      {/* MORE BOOKS */}
      {hasMoreBooks ? <div ref={ref} /> : null}
    </div>
  );
};

export default BooksList;
