import React from "react";
import { useRouter } from "next/router";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import querystring from "querystring";
import * as R from "ramda";

import appConfig from "../../../../appConfig";
import { maxLength } from "../../../../utils/form";
import Form from "../../../base/Form";
import LocationSelect from "../../selects/LocationSelect";

const maxTitleLength = maxLength(100);
const defaultValues = {
  title: "",
  location: appConfig.features.filter.defaultLocation,
  tags: undefined,
};

const setQueries = R.compose(
  R.concat("/books?"),
  querystring.stringify,
  R.pickBy(R.identity), // remove undefined values
);

const FilterForm = () => {
  const router = useRouter();
  const initialValues = {
    location: router.query.location,
    title: router.query.title,
  };

  // HANDLERS
  const handleFormSubmit = (values: any) => {
    const queries = {
      title: values.title,
      location: values.location,
    };

    router.push(setQueries(queries));
  };

  const handleReset = (form: any) => () => {
    // Reset all fields manually because `form.reset()` method reset values to initial state
    form.change("title", undefined);
    form.change("tags", undefined);
    form.submit();
  };

  // RENDER
  return (
    <Form
      initialValues={R.merge(defaultValues, initialValues)}
      validateOnBlur={false}
      onSubmit={handleFormSubmit}
    >
      {({ handleSubmit, form }) => {
        return (
          <form onSubmit={handleSubmit}>
            {/* FIELD: LOCATION */}
            <Form.Field name="location" title="Location:" id="filter-form-location">
              {({ input }: any) => <LocationSelect {...input} />}
            </Form.Field>

            {/* FIELD: TITLE */}
            <Form.Field
              name="title"
              title="Title:"
              id="filter-form-title"
              validate={maxTitleLength}
            >
              {({ input }: any) => (
                <FormControl type="text" size="sm" {...input} placeholder="e.g. React Native" />
              )}
            </Form.Field>

            {/* TAGS */}
            {/* <Form.Field name="tags" title="Tags:" id="filter-form-tags">
                            {({ input }) => (
                                <TagsList
                                    selected={input.value ? input.value.split(',') : []}
                                    items={tags}
                                    onSelect={selectedTags => {
                                        form.change('tags', selectedTags.join(','));
                                    }}
                                />
                            )}
                        </Form.Field> */}

            {/* BUTTONS */}
            <Form.Controls>
              <Button type="submit" variant="primary" size="sm">
                Search
              </Button>
              <Button size="sm" variant="link" onClick={handleReset(form)}>
                Reset
              </Button>
            </Form.Controls>
          </form>
        );
      }}
    </Form>
  );
};

export default FilterForm;
