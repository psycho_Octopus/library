import React from 'react';
import bemCl from 'bem-cl';
import NavB from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import ActiveLink from '../../base/ActiveLink';

import './Nav.scss';

const b = bemCl('nav');
const activeClassName = b('item', { state: 'active' }).toString();

const Nav = () => {
  return (
    <NavB className="mr-auto">
      <Navbar.Text className={b('item').toString()}>
        <ActiveLink href="/" activeClassName={activeClassName}>
          <span className={b('link')}>All books</span>
        </ActiveLink>
      </Navbar.Text>
      <Navbar.Text className={b('item').toString()}>
        <ActiveLink href="/order" activeClassName={activeClassName}>
          <span className={b('link')}>Order a book</span>
        </ActiveLink>
      </Navbar.Text>
      <Navbar.Text className={b('item').toString()}>
        <ActiveLink href="/login" activeClassName={activeClassName}>
          <span className={b('link')}>Login</span>
        </ActiveLink>
      </Navbar.Text>
    </NavB>
  );
};

export default Nav;
