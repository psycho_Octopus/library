import React from 'react';
import Form from 'react-bootstrap/Form';

import LocationsEnum from '../../../../../shared/enums/locations';

const options = [
  {
    value: LocationsEnum.Spb,
    title: 'Saint Peterburg',
  },
  {
    value: LocationsEnum.Prg,
    title: 'Prague',
  },
];

type Props = {};

const LocationSelect = (props: Props) => {
  return (
    <Form.Control size="sm" as="select">
      {options.map(option => (
        <option value={option.value} key={option.value}>
          {option.title}
        </option>
      ))}
    </Form.Control>
  );
};

export default LocationSelect;
