import React, { ReactNode } from 'react';
import Row from 'react-bootstrap/Row';

import Main from './components/Main';
import Search from './components/Search';
import './WithSearch.scss';

type Props = {
  children: ReactNode;
};

const WithSearch = ({ children }: Props) => {
  return <Row>{children}</Row>;
};

WithSearch.Main = Main;
WithSearch.Search = Search;

export default WithSearch;
