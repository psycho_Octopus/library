import React, { ReactNode } from 'react';
import Col from 'react-bootstrap/Col';

import { b } from '../constants';

type Props = {
  children: ReactNode;
};

const WithSearchSearch = ({ children }: Props) => {
  return (
    <Col md="3">
      <div className={b('search')}>{children}</div>
    </Col>
  );
};

export default WithSearchSearch;
