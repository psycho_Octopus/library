import React, { ReactNode } from 'react';
import Col from 'react-bootstrap/Col';

import { b } from '../constants';

type Props = {
  children: ReactNode;
};

const WithSearchMain = ({ children }: Props) => {
  return (
    <Col>
      <main className={b('main')}>{children}</main>
    </Col>
  );
};

export default WithSearchMain;
