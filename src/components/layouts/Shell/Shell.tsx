import React, { ReactNode } from "react";

import Wrapper from "../../base/Wrapper";

import Header from "../../modules/Header/Header";
import Footer from "./components/Footer";
import { b } from "./contants";
import "./Shell.scss";

type Props = {
  children: ReactNode;
};

const ShellLayout = ({ children }: Props) => {
  return (
    <div className={b()}>
      <Header />
      <Wrapper className={b("body").toString()}>{children}</Wrapper>
      <Footer />
    </div>
  );
};

export default ShellLayout;
