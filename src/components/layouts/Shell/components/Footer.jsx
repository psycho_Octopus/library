import React from 'react';

import Wrapper from '../../../base/Wrapper';

import { b } from '../contants';

const ShellLayoutFooter = () => {
  return (
    <footer className={b('footer')}>
      <Wrapper>footer</Wrapper>
    </footer>
  );
};

export default ShellLayoutFooter;
