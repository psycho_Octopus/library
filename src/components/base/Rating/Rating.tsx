import React from 'react';
import bemCl from 'bem-cl';
import { repeat } from 'ramda';

import './Rating.scss';

const b = bemCl('rating');

export enum Sizes {
  L = 'l',
  M = 'm',
  S = 's',
}

type Props = {
  value: number;
  size?: Sizes;
};

const Rating = ({ value, size = Sizes.M }: Props) => {
  return <div className={b({ size })}>{repeat('⭐', value)}</div>;
};

export default Rating;
