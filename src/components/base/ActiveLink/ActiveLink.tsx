import React, { ReactElement } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

type Props = {
  children: ReactElement;
  href: string;
  activeClassName?: string;
};

type ChildProps = {
  className?: string;
};

const ActiveLink = ({ children, ...props }: Props) => {
  const router = useRouter();
  const child = React.Children.only<ReactElement<ChildProps>>(children);

  let className = child.props.className || '';

  if (router.pathname === props.href && props.activeClassName) {
    className = `${className} ${props.activeClassName}`.trim();
  }

  // eslint-disable-next-line no-param-reassign
  delete props.activeClassName;

  return <Link {...props}>{React.cloneElement(child, { className })}</Link>;
};

export default ActiveLink;
