import React from 'react';
import { ifElse, uncurryN, includes, without, append } from 'ramda';
import bemCl from 'bem-cl';

import { Tag } from '../../../types/entities';

import './TagsList.scss';

type TagId = Tag['id'];
type Props = {
  className?: string | number;
  items: Tag[];
  selected?: TagId[];
  onSelect: (selectedTags: string[]) => void;
};
type ToggleItem = (item: TagId, selected: TagId[]) => TagId[];

const b = bemCl('tags-list');

const TagsList = ({ className = '', items = [], selected = [], onSelect }: Props) => {
  const toggleItem: ToggleItem = uncurryN(2, (item: TagId) =>
    ifElse(includes(item), without([item]), append(item)),
  );
  const isItemSelected = (itemId: TagId) => selected.includes(itemId);

  const handleItemSelect = (item: TagId) => {
    onSelect(toggleItem(item, selected));
  };

  return (
    <span className={b().mix(className)}>
      {items.map(item => (
        <button
          className={b('item', { selected: isItemSelected(item.id) })}
          type="button"
          onClick={handleItemSelect.bind(null, item.id)}
          key={item.id}
        >
          {item.title}
        </button>
      ))}
    </span>
  );
};

export default TagsList;
