import React, { ReactNode } from "react";
import bemCl from "bem-cl";

import "./Wrapper.scss";

const b = bemCl("wrapper");

type Props = {
  children: ReactNode;
  className?: string;
};

const Wrapper = ({ className = "", children }: Props) => {
  return <div className={b().mix(className)}>{children}</div>;
};

export default Wrapper;
