import React, { ReactText } from 'react';

type Props = {
  children: ReactText;
};

const DocTitle = ({ children }: Props) => {
  return <title>{children} | Library</title>;
};

export default DocTitle;
