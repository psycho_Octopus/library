import React, { ReactNode } from 'react';
import { Form as FinalForm, FormProps } from 'react-final-form';

import Field from './components/Field';
import Controls from './components/Controls';
import Row from './components/Row';
import './Form.scss';

type Props = {
  children: FormProps['render'];
} & Omit<FormProps, 'render'>;

const Form = ({ children, ...props }: Props) => {
  return <FinalForm render={children} onSubmit={props.onSubmit} {...props} />;
};

Form.Field = Field;
Form.Controls = Controls;
Form.Row = Row;

export default Form;
