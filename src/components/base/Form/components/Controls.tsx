import React, { ReactNode } from 'react';

import { b } from '../constants';

type Props = {
  children: ReactNode;
};

const FormControls = ({ children }: Props) => {
  return <div className={b('controls').toString()}>{children}</div>;
};

export default FormControls;
