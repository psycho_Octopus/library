import React from 'react';
import Form from 'react-bootstrap/Form';

type Props = {
    children: React.ReactElement | React.ReactElement[];
};

const FormControls = ({ children }: Props) => {
    return <Form.Row>{children}</Form.Row>;
};

export default FormControls;
