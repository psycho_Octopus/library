import React from 'react';
import { string, func, arrayOf, oneOfType } from 'prop-types';
import { Field } from 'react-final-form';
import Form from 'react-bootstrap/Form';

import { b } from '../constants';

const FormField = ({ children, ...props }) => {
  const { name, title, id, validate } = props;

  return (
    <Field
      name={name}
      validate={validate}
      render={({ input, meta }) => (
        <Form.Group controlId={id}>
          <Form.Label className={b('label').toString()}>{title}</Form.Label>
          {children({ input, meta, props })}
        </Form.Group>
      )}
    />
  );
};

FormField.propTypes = {
  children: func.isRequired,
  name: string.isRequired,
  title: string,
  id: string.isRequired,
  validate: oneOfType([func, arrayOf(func)]),
};

FormField.defaultProps = {
  title: '',
  validate: undefined,
};

export default FormField;
